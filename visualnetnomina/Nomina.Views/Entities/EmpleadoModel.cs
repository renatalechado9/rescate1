﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomina.Views.Entities
{
    class EmpleadoModel
    {
        private static List<Empleado> ListEmpleado = new List<Empleado>();

        public static List<Empleado> GetListEmpleado()
        {
            return ListEmpleado;
        }

        public static void Populate()
        {


            ListEmpleado = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(Nomina.Views.Properties.Resources.empleadodata));
        }
    }
}
