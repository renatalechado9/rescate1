﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomina.Views.Entities
{
    class Empleado
    {
        private int id;
        private string inss;
        private string cedula;
        private string nombres;
        private string apellidos;
        private string direccion;
        private string tconvencional;
        private string tcelular;
        private SEXO sexo;
        private double salario;

        public Empleado() { }

        public Empleado(int id, string inss, string cedula, string nombres, string apellidos, string direccion, string tconvencional, string tcelular, SEXO sexo, double salario)
        {
            this.id = id;
            this.inss = inss;
            this.cedula = cedula;
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.direccion = direccion;
            this.tconvencional = tconvencional;
            this.tcelular = tcelular;
            this.sexo = sexo;
            this.Salario = salario;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Inss
        {
            get
            {
                return inss;
            }

            set
            {
                inss = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public string Nombres
        {
            get
            {
                return nombres;
            }

            set
            {
                nombres = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Tconvencional
        {
            get
            {
                return tconvencional;
            }

            set
            {
                tconvencional = value;
            }
        }

        public string Tcelular
        {
            get
            {
                return tcelular;
            }

            set
            {
                tcelular = value;
            }
        }

        internal SEXO Sexo
        {
            get
            {
                return sexo;
            }

            set
            {
                sexo = value;
            }
        }

        public double Salario
        {
            get
            {
                return salario;
            }

            set
            {
                salario = value;
            }
        }

        public enum SEXO
        {
            FEMENINO, MASCULINO
        }

        public override string ToString()
        {
            return Nombres + " " + Apellidos;
        }

    }
}
