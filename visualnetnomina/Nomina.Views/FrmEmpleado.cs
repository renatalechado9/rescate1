﻿using Nomina.Views.Entities;
using Nomina.Business.Implements;
using Nomina.Views.Extensiones;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmEmpleado : Form
    {
        private DataSet dsEmpleado;
        private BindingSource bsEmpleado;
        public DataSet DsEmpleado
        {
            get
            {
                return dsEmpleado;
            }

            set
            {
                dsEmpleado = value;
            }
        }
        public bool flag;
        public FrmEmpleado()
        {
            bsEmpleado = new BindingSource();
            InitializeComponent();
        }
        
        

        private void FrmEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleado.DataSource = DsEmpleado;
            bsEmpleado.DataMember = DsEmpleado.Tables["Empleado"].TableName;
            dgvempleado.DataSource = bsEmpleado;
            dgvempleado.AutoGenerateColumns = true;
        }
    }
}
