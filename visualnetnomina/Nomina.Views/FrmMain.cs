﻿
using Nomina.Business.Implements;
using Nomina.Views.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmMain : Form
    {
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private List<Entities.Empleado> empleados;
        private BindingSource bsEmpleados;
        public FrmMain()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            bsEmpleados = new BindingSource();
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            //empleados = daoEmpleadoImplements.All();

            //empleados.ForEach(ep => {
            //    dsNomina.Tables["Empleado"].Rows.Add(ep.EmpleadoAsArray());
            //});

            EmpleadoModel.Populate();
            foreach (Entities.Empleado emp in EmpleadoModel.GetListEmpleado())
            {
                dsNomina.Tables["Empleado"].Rows.Add(emp.Id, emp.Cedula,emp.Nombres, emp.Apellidos, emp.Direccion,
                    emp.Tconvencional, emp.Tcelular, emp.Salario,emp.Inss, emp.Sexo);
            }
            bsEmpleados.DataSource = dsNomina;
            bsEmpleados.DataMember = dsNomina.Tables["Empleado"].TableName;

            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmEmpleado frmEmpleado = new FrmEmpleado();
            frmEmpleado.DsEmpleado= dsNomina;
            frmEmpleado.ShowDialog(this);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmEmpleado frmEmpleado = new FrmEmpleado();
            frmEmpleado.DsEmpleado = dsNomina;
            frmEmpleado.ShowDialog(this);
        }

        private void promediobtn_Click(object sender, EventArgs e)
        {
            double promedio = 0;
            foreach (Entities.Empleado emp in EmpleadoModel.GetListEmpleado())
            {
                promedio += emp.Salario;
            }
            promedio /= EmpleadoModel.GetListEmpleado().Count;
            MessageBox.Show("El promedio de salarios es de :" + promedio,"Mensaje de informacion");
        }

        private void maprombtn_Click(object sender, EventArgs e)
        {
            int cantidad = 0;
            double promedio = 0;
            foreach (Entities.Empleado emp in EmpleadoModel.GetListEmpleado())
            {
                promedio += emp.Salario;
            }
            promedio /= EmpleadoModel.GetListEmpleado().Count;

            foreach (Entities.Empleado emp in EmpleadoModel.GetListEmpleado())
            {
                if(emp.Salario >= promedio)
                {
                    cantidad++;
                }
            }
            MessageBox.Show("La cantidad de personas que superan el salario promedio es : " + cantidad, "Mensaje de informacion");
        }

        private void mayorbtn_Click(object sender, EventArgs e)
        {
            List<Empleado> empl = EmpleadoModel.GetListEmpleado();
            empl.OrderBy(o => o.Salario).ToList();
            dsNomina.Clear();
            for (int i = 0; i< 5; i++)
            {
                
                dsNomina.Tables["Empleado"].Rows.Add(empl[i].Id, empl[i].Cedula, empl[i].Nombres, empl[i].Apellidos, empl[i].Direccion,
                    empl[i].Tconvencional, empl[i].Tcelular, empl[i].Salario, empl[i].Inss, empl[i].Sexo);;
            }
            bsEmpleados.DataSource = dsNomina;
            bsEmpleados.DataMember = dsNomina.Tables["Empleado"].TableName;

            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        private void menorbtn_Click(object sender, EventArgs e)
        {
            List<Empleado> empl = EmpleadoModel.GetListEmpleado();
            empl.OrderBy(o => o.Salario).ToList();
            dsNomina.Clear();
            for (int i = empl.Count-1; i < empl.Count-6; i--)
            {

                dsNomina.Tables["Empleado"].Rows.Add(empl[i].Id, empl[i].Cedula, empl[i].Nombres, empl[i].Apellidos, empl[i].Direccion,
                    empl[i].Tconvencional, empl[i].Tcelular, empl[i].Salario, empl[i].Inss, empl[i].Sexo); ;
            }
            bsEmpleados.DataSource = dsNomina;
            bsEmpleados.DataMember = dsNomina.Tables["Empleado"].TableName;

            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }
    }
}
